import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './components/Header';
import Section from './components/Section';
import { DataProvider } from './components/Context';
// import "./App.css";
// import { Login, Signup } from "./components/login/index";
// import Register from './components/Register'
// import Login from  './component/Login';



class App extends React.Component {
  render() {
    return (
      <DataProvider>
        <div className="app">

          {/* <Login /> */}
          <Router>
            <Header />
            <Section />
          </Router>
        </div>

      </DataProvider>
      
      
    );
  }
}

export default App;
