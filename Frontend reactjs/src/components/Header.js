import React, { Component } from 'react'
// import {container} from 'react-bootstrap'
import Menu from './png/bars-solid.png'
import Close from './png/times-solid.png'
import CartIcon from './png/shopping-cart-solid.png'
import {Link} from 'react-router-dom'
import './css/Header.css'
import {DataContext} from './Context'



export class Header extends Component {
    static contextType = DataContext;

    state = {
        toggle: false
    }

    menuToggle = () =>{
        this.setState({toggle: !this.state.toggle})
    }


    render() {
        const {toggle} = this.state;
        const {cart} = this.context;
        return (
            <header>
                <div className="menu" onClick={this.menuToggle}>
                    <img src={Menu} alt="" width="20"/>
                </div>
                <div class='logo'>
                    <h1>Spotmart</h1>
                </div>
                <nav>
                    <ul className={toggle ? "toggle" : ""}>
                    <li><Link to="/product">Product</Link></li>
                        {/* <li><Link to="/">home</Link></li>
                         <li><Link to="/contact">Contact</Link></li>
                         <li><Link to="/about">About</Link></li> 
                       <div class="float-right"> <li><Link to="/login">Login</Link></li>
                       <li><Link to="/register">Register</Link></li> */} 
                       <li className="close" onClick={this.menuToggle}>
                            <img src={Close} alt="" width="20"/>
                        </li>
                    </ul>
                    <div className="nav-cart">
                        <span>{cart.length}</span>
                        <Link to="/cart">
                            <img src={CartIcon} alt="" width="30"/>
                        </Link>
                    </div>
                </nav>
            </header>
        )
    }
}

export default Header
