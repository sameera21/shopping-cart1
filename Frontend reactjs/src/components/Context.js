import React, { Component } from 'react'

export const DataContext = React.createContext();

export class DataProvider extends Component {

    state = {
        products: [
            {
                "_id": "1",
                "title": "Women Plain Lycra TOP",
                "src": "https://images-na.ssl-images-amazon.com/images/I/61GrsG0nL6L._UL1484_.jpg",
                "description": " Casual || Party || Beach || Formal",
                "content": "Team it with a pair of jeggings, Jeans, Shorts and flats to step out and  designed for comfort and style",
                "price": 450,
                // "colors": ["red"],
                "count": 1
            },
            {
                "_id": "2",
                "title": "Women Regular fit T-Shirt",
                "src": "https://images-na.ssl-images-amazon.com/images/I/71nQTEVzpnL._UL1500_.jpg",
                "description": "Casual || Party || Beach || Formal",
                "content": "Team it with a pair of jeggings, Jeans, Shorts and flats to step out and  designed for comfort and style",
                "price": 350,
                "colors": ["Black"],
                "count": 1
            },
            {
                "_id": "3",
                "title": "Women  Regular cotton T-shirt",
                "src": "https://m.media-amazon.com/images/I/81Uu651LoAL._UL1500_.jpg",
                "description": " Casual || Party || Beach || Formal",
                "content": "Team it with a pair of jeggings, Jeans, Shorts and flats to step out and  designed for comfort and style",
                "price": 550,
                "colors": ["olive"],
                "count": 1
            },
            {
                "_id": "4",
                "title": " Girls Hoodie T-Shirt Top",
                "src": "https://images-na.ssl-images-amazon.com/images/I/616zc%2BC623L._UL1500_.jpg",
                "description": "Casual || Party || Beach || Formal",
                "content": "Team it with a pair of jeggings, Jeans, Shorts and flats to step out and designed for comfort and style",
                "price": 750,
                "colors": ["Darkgreen"],
                "count": 1
            },
            {
                "_id": "5",
                "title": "Turtle Neck Cotton Tshirt for Women ",
                "src": "https://m.media-amazon.com/images/I/51SpnVi04eL._UL1202_.jpg",
                "description": "Formal || Beach || Casual",
                "content": "Team it with a pair of jeggings, Jeans, Shorts and flats to step out and designed for comfort and style",
                "price": 610,
                "colors": ["blue"],
                "count": 1
            },
            {
                "_id": "6",
                "title": "Women's  Solid Lace Net Top ",
                "src": "https://images-na.ssl-images-amazon.com/images/I/81vKNPzcQoL._UL1500_.jpg",
                "description": "Formal || Beach || party",
                "content": "Team it with a pair of jeggings, Jeans, Shorts and flats to step out and designed for comfort and style",
                "price": 459,
                "colors": ["red"],
                "count": 1
            }
        ],
        cart: [],
        total: 0
        
    };

    addCart = (id) =>{
        const {products, cart} = this.state;
        const check = cart.every(item =>{
            return item._id !== id
        })
        if(check){
            const data = products.filter(product =>{
                return product._id === id
            })
            this.setState({cart: [...cart,...data]})
        }else{
            alert('Added Succesfully')
            
        }
    };

    reduction = id =>{
        const { cart } = this.state;
        cart.forEach(item =>{
            if(item._id === id){
                item.count === 1 ? item.count = 1 : item.count -=1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };

    increase = id =>{
        const { cart } = this.state;
        cart.forEach(item =>{
            if(item._id === id){
                item.count += 1;
            }
        })
        this.setState({cart: cart});
        this.getTotal();
    };

    removeProduct = id =>{
        if(window.confirm("Remove from cart?")){
            const {cart} = this.state;
            cart.forEach((item, index) =>{
                if(item._id === id){
                    cart.splice(index, 1)
                }
            })
            this.setState({cart: cart});
            this.getTotal();
        }
       
    };

    getTotal = ()=>{
        const{cart} = this.state;
        const res = cart.reduce((prev, item) => {
            return prev + (item.price * item.count);
        },0)
        this.setState({total: res})
    };
    
    componentDidUpdate(){
        localStorage.setItem('dataCart', JSON.stringify(this.state.cart))
        localStorage.setItem('dataTotal', JSON.stringify(this.state.total))
    };

    componentDidMount(){
        const dataCart = JSON.parse(localStorage.getItem('dataCart'));
        if(dataCart !== null){
            this.setState({cart: dataCart});
        }
        const dataTotal = JSON.parse(localStorage.getItem('dataTotal'));
        if(dataTotal !== null){
            this.setState({total: dataTotal});
        }
    }
   

    render() {
        const {products, cart,total} = this.state;
        const {addCart,reduction,increase,removeProduct,getTotal} = this;
        return (
            <DataContext.Provider 
            value={{products, addCart, cart, reduction,increase,removeProduct,total,getTotal}}>
                {this.props.children}
            </DataContext.Provider>
        )
    }
}


