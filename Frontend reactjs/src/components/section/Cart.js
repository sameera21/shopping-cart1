import React, { Component } from 'react'
import {DataContext} from '../Context'
import {Link} from 'react-router-dom'
// import Colors from './Colors'
import '../css/Details.css'
import '../css/Cart.css'

export class Cart extends Component {
    static contextType = DataContext;

    componentDidMount(){
        this.context.getTotal();
    }
    
    render() {
        const {cart,increase,reduction,removeProduct,total} = this.context;
        if(cart.length === 0){
            return <h2 style={{textAlign:"center"}}>Please Add something into the cart</h2>
        }else{
            return (
                <>
                    {
                        cart.map(item =>(
                            <div className="details cart" key={item._id}>
                                <img src={item.src} alt=""/>
                                <div className="box">
                                    <div className="row">
                                        <h2>{item.title}</h2>
                                        {/* <span><h3>Rs.{item.price * item.count}</h3></span> */}
                                    </div> 
                                    {/* <Colors colors={item.colors}/> */}
                                    <p>{item.description}</p>
                                    <p>{item.content}</p>
                                    <div className="amount">
                                        <button className="count" onClick={() => reduction(item._id)}> - </button>
                                        <span>{item.count}</span>
                                        <button className="count" onClick={() => increase(item._id)}> + </button>
                                    </div>
                                </div>
                                <div className="delete" onClick={() => removeProduct(item._id)}>X</div>
                            </div>
                        ))
                    }
                    <div class="total">
                            <h3>Total: Rs.{total}</h3>
                            <div class="float-right">
                            <Link to="/checkout">Checkout</Link>
                            </div>

                    </div>
                        
                        
                    
                </>
                )
            }
        }
}

export default Cart
